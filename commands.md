# Commands and run
## Create the new CA
`./scepserver-darwin-arm64 ca -init`

This command creates the necessary cryptographic materials for the CA to function :
- private key = ca.key
- public key certificate = ca.pem


## Start the server
`./scepserver-darwin-arm64 -depot depot -port 2024 challenge=password`


## Cient request : 
`./scepclient-darwin-arm64 -private-key client.key -server-url=http://127.0.0.1:2024/scep -challenge=password`

If the client.key doesn't exist, the client will create a new rsa private key. Must be in PEM format.


# Example of run

## Serveur: 
noahbarbe@mac-noah scep % `./scepserver-darwin-arm64 ca -init`

## Initializing new CA
noahbarbe@mac-noah scep % `./scepserver-darwin-arm64 -depot depot -port 2024 -challenge=password`


## Client :

noahbarbe@mac-noah scep % `./scepclient-darwin-arm64 -private-key client.key -server-url=http://127.0.0.1:2024/scep -challenge=password`
