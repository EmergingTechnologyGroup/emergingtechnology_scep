# What is scep
## Description
Our project aims to delve deep into the Simple Certificate Enrollment Protocol (SCEP). As a widely used protocol in network security infrastructures, SCEP plays a crucial role in managing digital certificates and cryptographic keys.


To achieve this objective, we will:

- Understand the SCEP protocol and examine its components in detail, as well as its practical applications in network security.
- Explore the Public Key Infrastructure (PKI) and its various components, elucidating how they interplay with SCEP for secure certificate management.
- Dive into X.509 Certificates, comprehending their structure, attributes, and usage scenarios within the context of SCEP and PKI.

## Technologies
- Scep

# Commands and run
In the file commands.md, you can find commands that we used to make the pratice work. There is also the example and logs from our configuration.

# Credits
- Scep Documentation
  
# Developers
- Barbe Noah
- Rodrigues Tony
- Praz Eliot


